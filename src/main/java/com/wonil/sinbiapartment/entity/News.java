package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.enums.NewsState;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.news.NewsRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @Column(nullable = false, length = 20)
    private String title;

    @Column(nullable = false, length = 50)
    private String content;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 8)
    private NewsState newsState;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putNewsState(NewsState newsState) {
        this.newsState = newsState;
        this.dateUpdate = LocalDateTime.now();
    }

    private News(NewsBuilder builder) {
        this.resident = builder.resident;
        this.title = builder.title;
        this.content = builder.content;
        this.newsState = builder.newsState;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class NewsBuilder implements CommonModelBuilder<News> {
        private final Resident resident;
        private final String title;
        private final String content;
        private final NewsState newsState;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public NewsBuilder(Resident resident, NewsRequest request) {
            this.resident = resident;
            this.title = request.getTitle();
            this.content = request.getContent();
            this.newsState = NewsState.PLAN;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public News build() {
            return new News(this);
        }
    }
}
