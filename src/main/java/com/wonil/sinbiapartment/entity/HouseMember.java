package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.housemember.HouseMemberRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HouseMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 15)
    private String phone;

    private HouseMember(HouseMemberBuilder builder) {
        this.resident = builder.resident;
        this.name = builder.name;
        this.phone = builder.phone;
    }

    public static class HouseMemberBuilder implements CommonModelBuilder<HouseMember> {
        private final Resident resident;
        private final String name;
        private final String phone;

        public HouseMemberBuilder(Resident resident, HouseMemberRequest request) {
            this.resident = resident;
            this.name = request.getName();
            this.phone = request.getPhone();
        }

        @Override
        public HouseMember build() {
            return new HouseMember(this);
        }
    }
}
