package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.mycar.MyCarRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyCar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @Column(nullable = false, length = 10)
    private String myCarNum;

    @Column(nullable = false, length = 30)
    private String carModel;

    @Column(nullable = false, length = 10)
    private String carType;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public MyCar(MyCarBuilder builder) {
        this.resident = builder.resident;
        this.myCarNum = builder.myCarNum;
        this.carModel = builder.carModel;
        this.carType = builder.carType;
        this.dateCreate = builder.dateCreate;
    }

    public static class MyCarBuilder implements CommonModelBuilder<MyCar> {
        private final Resident resident;
        private final String myCarNum;
        private final String carModel;
        private final String carType;
        private final LocalDateTime dateCreate;

        public MyCarBuilder(Resident resident, MyCarRequest request) {
            this.resident = resident;
            this.myCarNum = request.getMyCarNum();
            this.carModel = request.getCarModel();
            this.carType = request.getCarType();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public MyCar build() {
            return new MyCar(this);
        }
    }
}
