package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.enums.Category;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.meeting.MeetingRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Meeting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20)
    private Category category;

    @Column(nullable = false, length = 20)
    private String title;

    @Column(nullable = false, length = 50)
    private String content;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private Meeting(MeetingBuilder builder) {
        this.resident = builder.resident;
        this.category = builder.category;
        this.title = builder.title;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MeetingBuilder implements CommonModelBuilder<Meeting> {
        private final Resident resident;
        private final Category category;
        private final String title;
        private final String content;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MeetingBuilder(Resident resident, MeetingRequest request) {
            this.resident = resident;
            this.category = request.getCategory();
            this.title = request.getTitle();
            this.content = request.getContent();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Meeting build() {
            return new Meeting(this);
        }
    }
}
