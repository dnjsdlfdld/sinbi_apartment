package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.complaincomments.ComplainCommentsRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainComments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complainId", nullable = false)
    private Complain complain;

    @Column(nullable = false, length = 50)
    private String content;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private ComplainComments(ServiceCounterCommentsBuilder builder) {
        this.resident = builder.resident;
        this.complain = builder.complain;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class ServiceCounterCommentsBuilder implements CommonModelBuilder<ComplainComments> {
        private final Resident resident;
        private final Complain complain;
        private final String content;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public ServiceCounterCommentsBuilder(Resident resident, Complain complain, ComplainCommentsRequest request) {
            this.resident = resident;
            this.complain = complain;
            this.content = request.getContent();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public ComplainComments build() {
            return new ComplainComments(this);
        }
    }
}
