package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.enums.ServiceState;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.complain.ComplainRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Complain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @Column(nullable = false, length = 20)
    private String title;

    @Column(nullable = false, length = 50)
    private String content;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 8)
    private ServiceState serviceState;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putServiceState(ServiceState serviceState) {
        this.serviceState = serviceState;
        this.dateUpdate = LocalDateTime.now();
    }

    private Complain(ComplainBuilder builder) {
        this.resident = builder.resident;
        this.title = builder.title;
        this.content = builder.content;
        this.serviceState = builder.serviceState;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class ComplainBuilder implements CommonModelBuilder<Complain> {
        private final Resident resident;
        private final String title;
        private final String content;
        private final ServiceState serviceState;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ComplainBuilder(Resident resident, ComplainRequest request) {
            this.resident = resident;
            this.title = request.getTitle();
            this.content = request.getContent();
            this.serviceState = ServiceState.RECEIVED;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }


        @Override
        public Complain build() {
            return new Complain(this);
        }
    }
}
