package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.meetingcomments.MeetingCommentsRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingComments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meetingId", nullable = false)
    private Meeting meeting;

    @Column(nullable = false, length = 50)
    private String content;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private MeetingComments(MeetingCommentsBuilder builder) {
        this.resident = builder.resident;
        this.meeting = builder.meeting;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MeetingCommentsBuilder implements CommonModelBuilder<MeetingComments> {
        private final Resident resident;
        private final Meeting meeting;
        private final String content;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MeetingCommentsBuilder(Resident resident, Meeting meeting, MeetingCommentsRequest request) {
            this.resident = resident;
            this.meeting = meeting;
            this.content = request.getContent();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public MeetingComments build() {
            return new MeetingComments(this);
        }
    }
}
