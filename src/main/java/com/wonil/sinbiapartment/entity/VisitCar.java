package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.enums.ApprovalState;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.visitcar.VisitCarRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VisitCar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ApiModelProperty(notes = "처리상태")
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 9)
    private ApprovalState approvalState;

    @ApiModelProperty(notes = "승인여부")
    @Column(nullable = false)
    private Boolean isApproval;

    @Column(nullable = false, length = 8)
    private String carNum;

    @Column(nullable = false)
    private LocalDate dateStart;

    @Column(nullable = false)
    private LocalDate dateEnd;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putVisitCarApproval(Boolean isApproval) {
        if (!isApproval) {
            this.isApproval = false;
            this.approvalState = ApprovalState.COMPANION;
        } else {
            this.isApproval = true;
            this.approvalState = ApprovalState.COMPLETE;
        }
    }

    public void putVisitCarState(ApprovalState approvalState) {
        this.approvalState = approvalState;
    }

    private VisitCar(VisitCarBuilder builder) {
        this.resident = builder.resident;
        this.approvalState = builder.approvalState;
        this.isApproval = builder.isApproval;
        this.carNum = builder.carNum;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.dateCreate = builder.dateCreate;
    }

    public static class VisitCarBuilder implements CommonModelBuilder<VisitCar> {
        private final Resident resident;
        private final ApprovalState approvalState;
        private final Boolean isApproval;
        private final String carNum;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final LocalDateTime dateCreate;

        public VisitCarBuilder(Resident resident, VisitCarRequest request) {
            this.resident = resident;
            this.approvalState = ApprovalState.BEFORE;
            this.isApproval = false;
            this.carNum = request.getCarNum();
            this.dateStart = request.getDateStart();
            this.dateEnd = request.getDateEnd();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public VisitCar build() {
            return new VisitCar(this);
        }
    }
}
