package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.newscomments.NewsCommentsRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsComments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "newsId", nullable = false)
    private News news;

    @Column(nullable = false, length = 50)
    private String content;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private NewsComments(NewsCommentsBuilder builder) {
        this.resident = builder.resident;
        this.news = builder.news;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class NewsCommentsBuilder implements CommonModelBuilder<NewsComments> {
        private final Resident resident;
        private final News news;
        private final String content;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public NewsCommentsBuilder(Resident resident, News news, NewsCommentsRequest request) {
            this.resident = resident;
            this.news = news;
            this.content = request.getContent();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public NewsComments build() {
            return new NewsComments(this);
        }
    }
}
