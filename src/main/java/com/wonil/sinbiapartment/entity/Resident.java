package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.enums.ResidentGroup;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.resident.ResidentRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;


@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Resident implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private ResidentGroup residentGroup;

    @ApiModelProperty(notes = "거주여부")
    @Column(nullable = false)
    private Boolean isEnable;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 50)
    private String address;

    @Column(nullable = false)
    private Integer apt_dong;

    @Column(nullable = false)
    private Integer apt_number;

    @Column(nullable = false, length = 15)
    private String phone;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putResidentRight(ResidentGroup residentGroup) {
        this.residentGroup = residentGroup;
    }

    private Resident(ResidentBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.isEnable = builder.isEnable;
        this.residentGroup = builder.residentGroup;
        this.name = builder.name;
        this.address = builder.address;
        this.apt_dong = builder.apt_dong;
        this.apt_number = builder.apt_number;
        this.phone = builder.phone;
        this.dateCreate = builder.dateCreate;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(residentGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnable;
    }

    public static class ResidentBuilder implements CommonModelBuilder<Resident> {
        private final String username;
        private final String password;
        private final Boolean isEnable;
        private final ResidentGroup residentGroup;
        private final String name;
        private final String address;
        private final Integer apt_dong;
        private final Integer apt_number;
        private final String phone;
        private final LocalDateTime dateCreate;

        public ResidentBuilder(ResidentGroup residentGroup, ResidentRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.isEnable = true;
            this.residentGroup = residentGroup;
            this.name = request.getName();
            this.address = request.getAddress();
            this.apt_dong = request.getApt_dong();
            this.apt_number = request.getApt_number();
            this.phone = request.getPhone();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public Resident build() {
            return new Resident(this);
        }
    }
}
