package com.wonil.sinbiapartment.entity;

import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import com.wonil.sinbiapartment.model.admincost.AdminCostRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdminCost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentId", nullable = false)
    private Resident resident;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDate dateStart;

    @Column(nullable = false)
    private LocalDate dateEnd;

    private AdminCost(AdminCostBuilder builder) {
        this.resident = builder.resident;
        this.price = builder.price;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
    }

    public static class AdminCostBuilder implements CommonModelBuilder<AdminCost> {
        private final Resident resident;
        private final Double price;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;

        public AdminCostBuilder(Resident resident, AdminCostRequest request, LocalDate dateStart, LocalDate dateEnd) {
            this.resident = resident;
            this.price = request.getPrice();
            this.dateStart = dateStart;
            this.dateEnd = dateEnd;
        }

        @Override
        public AdminCost build() {
            return new AdminCost(this);
        }
    }
}
