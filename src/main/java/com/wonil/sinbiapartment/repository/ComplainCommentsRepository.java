package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.ComplainComments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComplainCommentsRepository extends JpaRepository<ComplainComments, Long> {
    List<ComplainComments> findAllByComplain_IdOrderByIdDesc(Long complainId);
}
