package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.Complain;
import com.wonil.sinbiapartment.enums.ServiceState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComplainRepository extends JpaRepository<Complain, Long> {
//    List<Complain> findAllByServiceState(ServiceState serviceState);
    List<Complain> findAllByTitleContainingOrderByIdDesc(String title);

    List<Complain> findAllByOrderByIdDesc();

    List<Complain> findAllByServiceStateOrderByIdDesc(ServiceState serviceState);
}
