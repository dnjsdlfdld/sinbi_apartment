package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.AdminCost;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface AdminCostRepository extends JpaRepository<AdminCost, Long> {
    AdminCost findByResident_IdAndDateStart(Long resident_id, LocalDate dateStart);
}
