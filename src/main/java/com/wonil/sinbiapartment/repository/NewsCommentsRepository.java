package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.NewsComments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NewsCommentsRepository extends JpaRepository<NewsComments, Long> {
    List<NewsComments> findAllByNews_IdOrderByIdDesc(Long newsId);
}
