package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.ComplainComments;
import com.wonil.sinbiapartment.entity.MeetingComments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MeetingCommentsRepository extends JpaRepository<MeetingComments, Long> {
    List<MeetingComments> findAllByMeeting_IdOrderByIdDesc(Long complainId);
}
