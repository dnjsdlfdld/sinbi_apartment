package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.Meeting;
import com.wonil.sinbiapartment.enums.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MeetingRepository extends JpaRepository<Meeting, Long> {
    List<Meeting> findAllByCategoryOrderByIdDesc(Category category);
}
