package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.News;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.enums.ResidentGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NewsRepository extends JpaRepository<News, Long> {

//    News findByResident_IdAndResident_ResidentGroup(Long resident_id, ResidentGroup residentGroup);
}
