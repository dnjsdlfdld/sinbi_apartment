package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.HouseMember;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HouseMemberRepository extends JpaRepository<HouseMember, Long> {
    List<HouseMember> findAllByResident_Id(Long resident_id);
}
