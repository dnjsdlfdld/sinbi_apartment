package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.MyCar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MyCarRepository extends JpaRepository<MyCar, Long> {
    List<MyCar> findAllByResident_Id(Long residentId);
}
