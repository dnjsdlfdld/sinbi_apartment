package com.wonil.sinbiapartment.repository;

import com.wonil.sinbiapartment.entity.VisitCar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VisitCarRepository extends JpaRepository<VisitCar, Long> {
    List<VisitCar> findAllByResident_Id(Long resident_id);
}
