package com.wonil.sinbiapartment.model.news;

import com.wonil.sinbiapartment.entity.News;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsListItem {
    private Long newsId;
    private String title;
    private String newsState;

    private NewsListItem(NewsListItemBuilder builder) {
        this.newsId = builder.newsId;
        this.title = builder.title;
        this.newsState = builder.newsState;
    }

    public static class NewsListItemBuilder implements CommonModelBuilder<NewsListItem> {
        private final Long newsId;
        private final String title;
        private final String newsState;

        public NewsListItemBuilder(News news) {
            this.newsId = news.getId();
            this.title = news.getTitle();
            this.newsState = news.getNewsState().getName();
        }

        @Override
        public NewsListItem build() {
            return new NewsListItem(this);
        }
    }
}
