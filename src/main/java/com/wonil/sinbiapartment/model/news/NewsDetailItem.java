package com.wonil.sinbiapartment.model.news;

import com.wonil.sinbiapartment.entity.News;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsDetailItem {
    private Long newsId;
    private String manageName;
    private String title;
    private String content;
    private LocalDateTime dateCreate;

    private NewsDetailItem(NewsDetailItemBuilder builder) {
        this.newsId = builder.newsId;
        this.manageName = builder.manageName;
        this.title = builder.title;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
    }

    public static class NewsDetailItemBuilder implements CommonModelBuilder<NewsDetailItem> {
        private final Long newsId;
        private final String manageName;
        private final String title;
        private final String content;
        private final LocalDateTime dateCreate;

        public NewsDetailItemBuilder(News news) {
            this.newsId = news.getId();
            this.manageName = news.getResident().getName();
            this.title = news.getTitle();
            this.content = news.getContent();
            this.dateCreate = news.getDateCreate();
        }

        @Override
        public NewsDetailItem build() {
            return new NewsDetailItem(this);
        }
    }
}
