package com.wonil.sinbiapartment.model.meeting;

import com.wonil.sinbiapartment.enums.Category;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingCategoryItem {
    private Category category;
    private String categoryName;
    private String categoryIntroduce;
    private String imgUrl;

    private MeetingCategoryItem(MeetingCategoryItemBuilder builder) {
        this.category = builder.category;
        this.categoryName = builder.categoryName;
        this.categoryIntroduce = builder.categoryIntroduce;
        this.imgUrl = builder.imgUrl;
    }

    public static class MeetingCategoryItemBuilder implements CommonModelBuilder<MeetingCategoryItem> {
        private final Category category;
        private final String categoryName;
        private final String categoryIntroduce;
        private final String imgUrl;

        public MeetingCategoryItemBuilder(Category category) {
            this.category = category;
            this.categoryName = category.getName();
            this.categoryIntroduce = category.getIntroduce();
            this.imgUrl = category.getImgUrl();
        }

        @Override
        public MeetingCategoryItem build() {
            return new MeetingCategoryItem(this);
        }
    }
}
