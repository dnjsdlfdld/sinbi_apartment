package com.wonil.sinbiapartment.model.meeting;

import com.wonil.sinbiapartment.entity.Meeting;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingListItem {
    private Long id;
    private String title;

    private MeetingListItem(MeetingListItemBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
    }

    public static class MeetingListItemBuilder implements CommonModelBuilder<MeetingListItem> {
        private final Long id;
        private final String title;

        public MeetingListItemBuilder(Meeting meeting) {
            this.id = meeting.getId();
            this.title = meeting.getTitle();
        }

        @Override
        public MeetingListItem build() {
            return new MeetingListItem(this);
        }
    }
}
