package com.wonil.sinbiapartment.model.meeting;

import com.wonil.sinbiapartment.entity.Meeting;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingDetail {
    private Long meetingId;
    private String residentFullName;
    private String title;
    private String content;
    private LocalDateTime dateCreate;

    private MeetingDetail(MeetingDetailBuilder builder) {
        this.meetingId = builder.meetingId;
        this.residentFullName = builder.residentFullName;
        this.title = builder.title;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
    }

    public static class MeetingDetailBuilder implements CommonModelBuilder<MeetingDetail> {
        private final Long meetingId;
        private final String residentFullName;
        private final String title;
        private final String content;
        private final LocalDateTime dateCreate;

        public MeetingDetailBuilder(Meeting meeting) {
            this.meetingId = meeting.getId();
            this.residentFullName = meeting.getResident().getName() + "("+ meeting.getResident().getApt_dong() +"동)";
            this.title = meeting.getTitle();
            this.content = meeting.getContent();
            this.dateCreate = meeting.getDateCreate();
        }

        @Override
        public MeetingDetail build() {
            return new MeetingDetail(this);
        }
    }
}
