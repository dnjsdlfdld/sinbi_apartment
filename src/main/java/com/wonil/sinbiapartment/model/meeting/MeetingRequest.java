package com.wonil.sinbiapartment.model.meeting;

import com.wonil.sinbiapartment.enums.Category;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MeetingRequest {
    @NotNull
    @ApiModelProperty(required = true)
    private Category category;

    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(required = true)
    private String title;

    @NotNull
    @Length(min = 1, max = 50)
    @ApiModelProperty(required = true)
    private String content;
}
