package com.wonil.sinbiapartment.model.meetingcomments;

import com.wonil.sinbiapartment.entity.MeetingComments;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MeetingCommentsItem {
    private String residentFullName;
    private String content;

    private MeetingCommentsItem(MeetingCommentsItemBuilder builder) {
        this.residentFullName = builder.residentFullName;
        this.content = builder.content;
    }

    public static class MeetingCommentsItemBuilder implements CommonModelBuilder<MeetingCommentsItem> {
        private final String residentFullName;
        private final String content;

        public MeetingCommentsItemBuilder(MeetingComments meetingComments) {
            this.residentFullName = meetingComments.getResident().getName()
                    + "(" + meetingComments.getResident().getResidentGroup().getName() + ")";
            this.content = meetingComments.getContent();
        }

        @Override
        public MeetingCommentsItem build() {
            return new MeetingCommentsItem(this);
        }
    }
}
