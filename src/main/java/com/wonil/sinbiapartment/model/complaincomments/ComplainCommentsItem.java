package com.wonil.sinbiapartment.model.complaincomments;

import com.wonil.sinbiapartment.entity.Complain;
import com.wonil.sinbiapartment.entity.ComplainComments;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainCommentsItem {
    private String residentFullName;
    private String content;

    private ComplainCommentsItem(ComplainCommentsItemBuilder builder) {
        this.residentFullName = builder.residentFullName;
        this.content = builder.content;
    }

    public static class ComplainCommentsItemBuilder implements CommonModelBuilder<ComplainCommentsItem> {
        private final String residentFullName;
        private final String content;

        public ComplainCommentsItemBuilder(ComplainComments complainComments) {
            this.residentFullName = complainComments.getResident().getName()
                    + "(" + complainComments.getResident().getResidentGroup().getName() + ")";
            this.content = complainComments.getContent();
        }

        @Override
        public ComplainCommentsItem build() {
            return new ComplainCommentsItem(this);
        }
    }
}
