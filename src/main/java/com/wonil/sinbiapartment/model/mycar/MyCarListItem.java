package com.wonil.sinbiapartment.model.mycar;

import com.wonil.sinbiapartment.entity.MyCar;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyCarListItem {
    private String myCarNum;
    private String carModel;
    private String carType;

    private MyCarListItem(MyCarListItemBuilder builder) {
        this.myCarNum = builder.myCarNum;
        this.carModel = builder.carModel;
        this.carType = builder.carType;
    }

    public static class MyCarListItemBuilder implements CommonModelBuilder<MyCarListItem> {
        private final String myCarNum;
        private final String carModel;
        private final String carType;

        public MyCarListItemBuilder(MyCar myCar) {
            this.myCarNum = myCar.getMyCarNum();
            this.carModel = myCar.getCarModel();
            this.carType = myCar.getCarType();
        }

        @Override
        public MyCarListItem build() {
            return new MyCarListItem(this);
        }
    }
}
