package com.wonil.sinbiapartment.model.mycar;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MyCarRequest {
    @NotNull
    @Length(min = 7, max = 8)
    @ApiModelProperty(required = true)
    private String myCarNum;

    @NotNull
    @Length(min = 2, max = 30)
    @ApiModelProperty(required = true)
    private String carModel;

    @NotNull
    @Length(min = 2, max = 10)
    @ApiModelProperty(required = true)
    private String carType;
}
