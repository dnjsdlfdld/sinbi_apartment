package com.wonil.sinbiapartment.model.admincost;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class AdminCostRequest {
    @NotNull
    @ApiModelProperty(required = true)
    private Double price;

}
