package com.wonil.sinbiapartment.model.admincost;

import com.wonil.sinbiapartment.entity.AdminCost;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.Month;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdminCostMonthItem {
    private Integer costMonth;
    private Double price;

    private AdminCostMonthItem(AdminCostMonthItemBuilder builder) {
        this.costMonth = builder.costMonth;
        this.price = builder.price;
    }

    public static class AdminCostMonthItemBuilder implements CommonModelBuilder<AdminCostMonthItem> {
        private final Integer costMonth;
        private final Double price;

        public AdminCostMonthItemBuilder(AdminCost adminCost) {
            this.costMonth = adminCost.getDateStart().getMonth().minus(1).getValue();
            this.price = adminCost.getPrice();
        }

        @Override
        public AdminCostMonthItem build() {
            return new AdminCostMonthItem(this);
        }
    }
}
