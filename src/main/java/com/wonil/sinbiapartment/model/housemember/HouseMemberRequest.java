package com.wonil.sinbiapartment.model.housemember;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HouseMemberRequest {
    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(required = true)
    private String name;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(required = true)
    private String phone;
}
