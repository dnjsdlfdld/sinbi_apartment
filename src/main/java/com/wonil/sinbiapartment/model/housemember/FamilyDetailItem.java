package com.wonil.sinbiapartment.model.housemember;

import com.wonil.sinbiapartment.entity.HouseMember;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FamilyDetailItem {
    private String houseMemberName;


    private FamilyDetailItem(FamilyDetailItemBuilder builder) {
        this.houseMemberName = builder.houseMemberName;
    }

    public static class FamilyDetailItemBuilder implements CommonModelBuilder<FamilyDetailItem> {
        private final String houseMemberName;

        public FamilyDetailItemBuilder(HouseMember houseMember) {
            this.houseMemberName = houseMember.getName();
        }

        @Override
        public FamilyDetailItem build() {
            return new FamilyDetailItem(this);
        }
    }
}
