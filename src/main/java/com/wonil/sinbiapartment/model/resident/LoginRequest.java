package com.wonil.sinbiapartment.model.resident;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LoginRequest {
    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(required = true)
    private String username;

    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(required = true)
    private String password;
}
