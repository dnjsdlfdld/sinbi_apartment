package com.wonil.sinbiapartment.model.resident;

import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginSecurityResponse {
    @ApiModelProperty(notes = "토큰")
    private String token;

    @ApiModelProperty(notes = "이름")
    private String name;

    private LoginSecurityResponse(LoginSecurityResponseBuilder builder) {
        this.token = builder.token;
        this.name = builder.name;
    }

    public static class LoginSecurityResponseBuilder implements CommonModelBuilder<LoginSecurityResponse> {
        private final String token;
        private final String name;

        public LoginSecurityResponseBuilder(String token, String name) {
            this.token = token;
            this.name = name;
        }

        @Override
        public LoginSecurityResponse build() {
            return new LoginSecurityResponse(this);
        }
    }
}
