package com.wonil.sinbiapartment.model.resident;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class MyInfoItem {
    private String residentName;
    private String phone;
    private String addressFullName;

    private MyInfoItem(MyInfoItemBuilder builder) {
        this.residentName = builder.residentName;
        this.phone = builder.phone;
        this.addressFullName = builder.addressFullName;
    }

    public static class MyInfoItemBuilder implements CommonModelBuilder<MyInfoItem> {
        private final String residentName;
        private final String phone;
        private final String addressFullName;

        public MyInfoItemBuilder(Resident resident) {
            this.residentName = resident.getName();
            this.phone = resident.getPhone();
            this.addressFullName = resident.getAddress() + " " + resident.getApt_dong() + "동" + " " + resident.getApt_number() + "호";
        }

        @Override
        public MyInfoItem build() {
            return new MyInfoItem(this);
        }
    }
}
