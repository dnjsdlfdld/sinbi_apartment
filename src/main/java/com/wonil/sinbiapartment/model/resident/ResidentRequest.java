package com.wonil.sinbiapartment.model.resident;

import com.wonil.sinbiapartment.enums.ResidentGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class ResidentRequest {
    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(required = true)
    private String username;

    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(required = true)
    private String password;

    @ApiModelProperty(notes = "비밀번호 확인", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String passwordRe;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(required = true)
    private String name;

    @NotNull
    @Length(min = 10, max = 50)
    @ApiModelProperty(required = true)
    private String address;

    @NotNull
    @ApiModelProperty(required = true)
    private Integer apt_dong;

    @NotNull
    @ApiModelProperty(required = true)
    private Integer apt_number;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(required = true)
    private String phone;
}
