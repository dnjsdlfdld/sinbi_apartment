package com.wonil.sinbiapartment.model.resident;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResidentNameItem {
    private String name;

    private ResidentNameItem(ResidentNameItemBuilder builder) {
        this.name = builder.name;
    }

    public static class ResidentNameItemBuilder implements CommonModelBuilder<ResidentNameItem> {
        private final String name;

        public ResidentNameItemBuilder(Resident resident) {
            this.name = resident.getName();
        }

        @Override
        public ResidentNameItem build() {
            return new ResidentNameItem(this);
        }
    }
}
