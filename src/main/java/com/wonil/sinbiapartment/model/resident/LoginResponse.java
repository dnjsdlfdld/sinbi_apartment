package com.wonil.sinbiapartment.model.resident;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    private Long residentId;

    private LoginResponse(LoginResponseBuilder builder) {
        this.residentId = builder.residentId;
    }

    public static class LoginResponseBuilder implements CommonModelBuilder<LoginResponse> {
        private final Long residentId;

        public LoginResponseBuilder(Resident resident) {
            this.residentId = resident.getId();
        }
        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
