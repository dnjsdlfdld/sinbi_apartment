package com.wonil.sinbiapartment.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult{ // <T> = 정해지지 않은 무언가
    private List<T> list; // List 이기 때문에 여러개일 것이다. 총 몇 페이지인지 ex)게시판

    private Long totalItemCount; // 몇 페이지가 될 지 모르기 때문에 가장 큰 정수를 넣어줌

    private Integer totalPage; // 페이지가 몇 백만번 째일 수 없음 Integer

    private Integer currentPage; // 현재 몇 번째 페이지인가?
}
