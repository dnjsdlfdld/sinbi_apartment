package com.wonil.sinbiapartment.model.visitcar;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class VisitCarRequest {
    @NotNull
    @Length(min = 7, max = 8)
    @ApiModelProperty(required = true)
    private String carNum;

    @NotNull
    @ApiModelProperty(required = true)
    private LocalDate dateStart;

    @NotNull
    @ApiModelProperty(required = true)
    private LocalDate dateEnd;
}
