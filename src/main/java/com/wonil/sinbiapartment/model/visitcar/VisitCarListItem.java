package com.wonil.sinbiapartment.model.visitcar;

import com.wonil.sinbiapartment.entity.VisitCar;
import com.wonil.sinbiapartment.enums.ApprovalState;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VisitCarListItem {
    private Long visitCarId;
    private String carNum;
    private String approvalState;
    private LocalDateTime dateCreate;

    private VisitCarListItem(VisitCarListItemBuilder builder) {
        this.visitCarId = builder.visitCarId;
        this.carNum = builder.carNum;
        this.approvalState = builder.approvalState;
        this.dateCreate = builder.dateCreate;
    }

    public static class VisitCarListItemBuilder implements CommonModelBuilder<VisitCarListItem> {
        private final Long visitCarId;
        private final String carNum;
        private final String approvalState;
        private final LocalDateTime dateCreate;

        public VisitCarListItemBuilder(VisitCar visitCar) {
            this.visitCarId = visitCar.getId();
            this.carNum = visitCar.getCarNum();
            this.approvalState = visitCar.getApprovalState().getName();
            this.dateCreate = visitCar.getDateCreate();
        }

        @Override
        public VisitCarListItem build() {
            return new VisitCarListItem(this);
        }
    }
}
