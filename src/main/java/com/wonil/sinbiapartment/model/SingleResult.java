package com.wonil.sinbiapartment.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult{  //SingleResult<T> = 어떤 타입을 줄거니 (Repository 연상)
    // <T> 미래에 들어올 무언가 = 냉장고 음식, 제네릭 = 무언가
    private T data;
}
