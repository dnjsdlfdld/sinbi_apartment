package com.wonil.sinbiapartment.model;

import lombok.Getter;
import lombok.Setter;
// model, Entity 는 wrapper 클래스가 들어감
@Getter
@Setter
public class CommonResult {
    private Boolean isSuccess;
    private Integer code;
    private String msg;

}
