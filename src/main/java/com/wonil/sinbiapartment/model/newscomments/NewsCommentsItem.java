package com.wonil.sinbiapartment.model.newscomments;

import com.wonil.sinbiapartment.entity.NewsComments;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NewsCommentsItem {
    private String residentFullName;
    private String content;

    private NewsCommentsItem(NewsCommentsItemBuilder builder) {
        this.residentFullName = builder.residentFullName;
        this.content = builder.content;
    }

    public static class NewsCommentsItemBuilder implements CommonModelBuilder<NewsCommentsItem> {
        private final String residentFullName;
        private final String content;

        public NewsCommentsItemBuilder(NewsComments newsComments) {
            this.residentFullName = newsComments.getResident().getName()
                    + "(" + newsComments.getResident().getResidentGroup().getName() + ")";
            this.content = newsComments.getContent();
        }

        @Override
        public NewsCommentsItem build() {
            return new NewsCommentsItem(this);
        }
    }
}
