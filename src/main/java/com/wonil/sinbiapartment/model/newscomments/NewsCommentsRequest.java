package com.wonil.sinbiapartment.model.newscomments;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class NewsCommentsRequest {
    @NotNull
    @Length(min = 1, max = 50)
    @ApiModelProperty(required = true)
    private String content;
}
