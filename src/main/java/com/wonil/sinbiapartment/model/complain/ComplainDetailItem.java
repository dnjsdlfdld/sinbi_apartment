package com.wonil.sinbiapartment.model.complain;

import com.wonil.sinbiapartment.entity.Complain;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainDetailItem {
    private Long complainId;
    private String residentFullName;
    private String title;
    private String content;
    private LocalDateTime dateCreate;

    private ComplainDetailItem(ComplainDetailItemBuilder builder) {
        this.complainId = builder.complainId;
        this.residentFullName = builder.residentFullName;
        this.title = builder.title;
        this.content = builder.content;
        this.dateCreate = builder.dateCreate;
    }

    public static class ComplainDetailItemBuilder implements CommonModelBuilder<ComplainDetailItem> {
        private final Long complainId;
        private final String residentFullName;
        private final String title;
        private final String content;
        private final LocalDateTime dateCreate;

        public ComplainDetailItemBuilder(Complain complain) {
            this.complainId = complain.getId();
            this.residentFullName = complain.getResident().getName() + "("+ complain.getResident().getApt_dong() +"동)";
            this.title = complain.getTitle();
            this.content = complain.getContent();
            this.dateCreate = complain.getDateCreate();
        }

        @Override
        public ComplainDetailItem build() {
            return new ComplainDetailItem(this);
        }
    }
}
