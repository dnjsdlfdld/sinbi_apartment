package com.wonil.sinbiapartment.model.complain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ComplainRequest {
    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(required = true)
    private String title;

    @NotNull
    @Length(min = 1, max = 50)
    @ApiModelProperty(required = true)
    private String content;

}
