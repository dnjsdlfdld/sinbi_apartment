package com.wonil.sinbiapartment.model.complain;

import com.wonil.sinbiapartment.entity.Complain;
import com.wonil.sinbiapartment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainListItem {
    private Long complainId;
    private String title;
    private String serviceState;

    private ComplainListItem(ComplainListItemBuilder builder) {
        this.complainId = builder.complainId;
        this.title = builder.title;
        this.serviceState = builder.serviceState;
    }

    public static class ComplainListItemBuilder implements CommonModelBuilder<ComplainListItem> {
        private final Long complainId;
        private final String title;
        private final String serviceState;

        public ComplainListItemBuilder(Complain complain) {
            this.complainId = complain.getId();
            this.title = complain.getTitle();
            this.serviceState = complain.getServiceState().getName();
        }

        @Override
        public ComplainListItem build() {
            return new ComplainListItem(this);
        }
    }
}
