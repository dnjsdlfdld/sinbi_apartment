package com.wonil.sinbiapartment.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;

    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManagerBean();
    }

    /*
    flutter 에서 header에 token 넣는법
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!; <-- 한줄 추가

    --- 아래 동일
     */

    // 퍼미션이 무엇인지 보기.
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용
                        .antMatchers("/v1/complain/new").hasAnyRole("USER") // 민원 등록
                        .antMatchers("/v1/complain/all").hasAnyRole("ADMIN", "USER") // 민원 전체리스트
                        .antMatchers("/v1/complain/received").hasAnyRole("ADMIN", "USER") // 민원(접수)리스트
                        .antMatchers("/v1/complain/ongoing").hasAnyRole("ADMIN", "USER") // 민원(진행중)리스트
                        .antMatchers("/v1/complain/complete").hasAnyRole("ADMIN", "USER") // 민원(완료)리스트
                        .antMatchers("/v1/complain/detail/{complainId}").hasAnyRole("ADMIN", "USER") // 민원 Detail
                        .antMatchers("/v1/visit-car/new").hasAnyRole("ADMIN", "USER") // 방문차량 등록
                        .antMatchers("/v1/visit-car/all").hasAnyRole("ADMIN", "USER") // 방문차량 신청 전체리스트
                        .antMatchers("/v1/house-member/family-detail").hasAnyRole("USER") // 세대주의 세대원 이름 불러오기
                        .antMatchers("/v1/resident/my-info").hasAnyRole("USER") // 내 정보 가져오기
                        .antMatchers("/v1/resident/name").hasAnyRole("USER") // 내 이름 가져오기
                        .antMatchers("/v1/house-member/new").hasAnyRole("ADMIN", "USER") // 세대원 등록
                        .antMatchers("/v1/complain-comments/comments/{complainId}").hasAnyRole("USER") // 민원 댓글 가져오기
                        .antMatchers("/v1/complain-comments/resident/complain/{complainId}").hasAnyRole("USER") // 민원 댓글작성
                        .antMatchers("/v1/news/new").hasAnyRole("ADMIN") // 아파트 소식 등록하기
                        .antMatchers("/v1/news/all").hasAnyRole("ADMIN", "USER") // 아파트 소식 전체 리스트 가져오기
                        .antMatchers("/v1/news/detail/{newsId}").hasAnyRole("ADMIN", "USER") // 아파트 소식 디테일 가져오기
                        .antMatchers("/v1/news/news-state/{newsId}").hasAnyRole("ADMIN") // 아파트 소식 상태 변경
                        .antMatchers("/v1/news-comments/resident/news/{newsId}").hasAnyRole("ADMIN", "USER") // 아파트 소식 댓글 작성
                        .antMatchers("/v1/news-comments/all/{newsId}").hasAnyRole("ADMIN", "USER") // 아파트 소식 댓글 가져오기
                        .antMatchers("/v1/my-car/resident/car-all").hasAnyRole("ADMIN", "USER") // 입주민용 본인 차량리스트 가져오기
                        .antMatchers("/v1/my-car/manager/{residentId}").hasAnyRole("ADMIN") // 관리자용 입주민의 차량 리스트 가져오기
                        .antMatchers("/v1/my-car/new/{residentId}").hasAnyRole("ADMIN") // 입주민 차량 등록
                        .antMatchers("/v1/admin-cost/new/{residentId}").hasAnyRole("ADMIN") // 관리자가 입주민의 관리비 등록
                        .antMatchers("/v1/admin-cost/cost/month").hasAnyRole("USER") // 입주민의 전월 관리비 숫자 가져오기
                        .antMatchers("/v1/meeting/category").hasAnyRole("ADMIN", "USER") // 카테고리 모임 가져오기
                        .antMatchers("/v1/meeting/new").hasAnyRole("USER") // 모임 글 등록하기
                        .antMatchers("/v1/meeting/category/name").hasAnyRole("USER") // 카테고리별 모임 게시글 가져오기
                        .antMatchers("/v1/meeting/category/{meetingId}").hasAnyRole("USER") // 모임 게시글 상세정보 가져오기
                .antMatchers("/v1/meeting-comments/resident/meeting/{meetingId}").hasAnyRole("USER") // 사용자가 모임게시글에 댓글달기
                .antMatchers("/v1/meeting-comments/comments/{meetingId}").hasAnyRole("USER") // 모임 게시글 댓글 가져오기
                        .antMatchers("/v1/login/**").permitAll() // 전체허용
                        .antMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN")
                        .antMatchers("/v1/auth-test/test-user").hasAnyRole("USER")
                        .antMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER")
                        .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
                .and()
                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                    .addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
