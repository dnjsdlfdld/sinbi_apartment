package com.wonil.sinbiapartment.exception;
// Custom
public class CCheckEnableException extends RuntimeException{ // 출구니까 출구의 기능을 상속 받아야함
    public CCheckEnableException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CCheckEnableException(String msg) {
        super(msg);
    }

    public CCheckEnableException() {
        super();
    }

}
