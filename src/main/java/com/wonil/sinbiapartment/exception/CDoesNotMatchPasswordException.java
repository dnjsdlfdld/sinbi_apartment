package com.wonil.sinbiapartment.exception;
// Custom
public class CDoesNotMatchPasswordException extends RuntimeException{ // 출구니까 출구의 기능을 상속 받아야함
    public CDoesNotMatchPasswordException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CDoesNotMatchPasswordException(String msg) {
        super(msg);
    }

    public CDoesNotMatchPasswordException() {
        super();
    }

}
