package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.Complain;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.enums.ServiceState;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CCheckComplainDetailException;
import com.wonil.sinbiapartment.exception.CCheckResidentException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.complain.ComplainDetailItem;
import com.wonil.sinbiapartment.model.complain.ComplainListItem;
import com.wonil.sinbiapartment.model.complain.ComplainRequest;
import com.wonil.sinbiapartment.repository.ComplainRepository;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ComplainService {
    private final ComplainRepository complainRepository;
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    public Complain getComplain(long complainId) {
        return complainRepository.findById(complainId).orElseThrow(CCheckComplainDetailException::new);
    }

    /**
     * 입주민이 민원 게시글 등록
     *
     * @param request 민원 게시글 Model
     */
    public void setComplain(ComplainRequest request) {
        Resident resident = getMemberData();
        Complain complain = new Complain.ComplainBuilder(resident, request).build();
        complainRepository.save(complain);
    }

    /**
     * 민원 게시글에 뿌려질 리스트 제목 가져오기(제목 검색)
     *
     * @return result
     */
    public ListResult<ComplainListItem> getComplainListName(String title) {
        List<Complain> complains = complainRepository.findAllByTitleContainingOrderByIdDesc(title);
        List<ComplainListItem> result = new LinkedList<>();

        complains.forEach(complain -> {
            ComplainListItem complainListItem = new ComplainListItem.ComplainListItemBuilder(complain).build();
            result.add(complainListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 민원 게시글에 뿌려질 리스트 제목 가져오기
     *
     * @return result 조건이 없는 모든 리스트
     */
    public ListResult<ComplainListItem> getComplainList() {
        List<Complain> complains = complainRepository.findAllByOrderByIdDesc();
        List<ComplainListItem> result = new LinkedList<>();

        complains.forEach(complain -> {
            ComplainListItem complainListItem = new ComplainListItem.ComplainListItemBuilder(complain).build();
            result.add(complainListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 상태가 접수인 데이터만 가져오기
     *
     * @return ListResult<ComplainListItem> 리스트 카운트 및 모델
     */
    public ListResult<ComplainListItem> getComplainReceivedState() {
        ServiceState selected = ServiceState.RECEIVED;
        List<Complain> received = complainRepository.findAllByServiceStateOrderByIdDesc(selected);
        List<ComplainListItem> result = new LinkedList<>();

        received.forEach(complain -> {
            ComplainListItem complainListItem = new ComplainListItem.ComplainListItemBuilder(complain).build();
            result.add(complainListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 상태가 진행중인 데이터만 가져오기
     *
     * @return ListResult<ComplainListItem> 리스트 카운트 및 모델
     */
    public ListResult<ComplainListItem> getComplainOngoingState() {
        ServiceState selected = ServiceState.ONGOING;
        List<Complain> onGoing = complainRepository.findAllByServiceStateOrderByIdDesc(selected);
        List<ComplainListItem> result = new LinkedList<>();

        onGoing.forEach(complain -> {
            ComplainListItem complainListItem = new ComplainListItem.ComplainListItemBuilder(complain).build();
            result.add(complainListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 상태가 완료인 데이터만 가져오기
     *
     * @return ListResult<ComplainListItem> 리스트 카운트 및 모델
     */
    public ListResult<ComplainListItem> getComplainCompleteState() {
        ServiceState selected = ServiceState.COMPLETE;
        List<Complain> complete = complainRepository.findAllByServiceStateOrderByIdDesc(selected);
        List<ComplainListItem> result = new LinkedList<>();

        complete.forEach(complain -> {
            ComplainListItem complainListItem = new ComplainListItem.ComplainListItemBuilder(complain).build();
            result.add(complainListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 민원 게시글 상세정보
     *
     * @param complainId 게시글 시퀀스
     * @return Model Builder 반환
     */
    public ComplainDetailItem getComplainDetail(long complainId) {
        Complain complain = complainRepository.findById(complainId).orElseThrow(CMissingDataException::new);
        return new ComplainDetailItem.ComplainDetailItemBuilder(complain).build();
    }

    /**
     * 관리자가 민원 게시글 상태 수정하기
     *
     * @param complainId   민원 게시글 시퀀스
     * @param serviceState 민원 게시글 상태
     */
    public void putComplainState(long complainId, ServiceState serviceState) {
        Complain complain = complainRepository.findById(complainId).orElseThrow(CCheckResidentException::new);
        complain.putServiceState(serviceState);
        complainRepository.save(complain);
    }
}
