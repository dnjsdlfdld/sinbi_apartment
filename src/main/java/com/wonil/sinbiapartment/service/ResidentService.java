package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.enums.ResidentGroup;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CCheckResidentException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.resident.MyInfoItem;
import com.wonil.sinbiapartment.model.resident.ResidentNameItem;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ResidentService {
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 해당 시퀀스 Record 반환
     * 입주민용
     *
     * @return Resident Record
     */
    public Resident getResident() {
        Resident resident = getMemberData();
        return residentRepository.findById(resident.getId()).orElseThrow(CCheckResidentException::new);
    }

    /**
     * 관리자용 resident record 가져오기
     *
     * @return Resident Record
     */
    public Resident getResidentManager(long residentId) {
        return residentRepository.findById(residentId).orElseThrow(CCheckResidentException::new);
    }

    /**
     * 로그인 회원명 가져오기
     *
     * @return
     */
    public ResidentNameItem getName() {
        Resident resident = getMemberData();
        ResidentNameItem result = new ResidentNameItem.ResidentNameItemBuilder(resident).build();
        return result;
    }

//    /**
//     * 관리자가 입주민 정보 등록하기
//     *
//     * @param request 회원 등록 Model
//     */
//    public void setResident(ResidentRequest request) {
//        Resident resident = new Resident.ResidentBuilder(request).build();
//        residentRepository.save(resident);
//    }

    public MyInfoItem getMyInfo() {
        Resident residentToken = getMemberData();
        Resident resident = residentRepository.findById(residentToken.getId()).orElseThrow(CMissingDataException::new);
        return new MyInfoItem.MyInfoItemBuilder(resident).build();
    }

    /**
     * 권한 변경
     *
     * @param residentId    회원 시퀀스
     * @param residentGroup 입주민 or 관리자 권한
     */
    public void putResidentAdmin(long residentId, ResidentGroup residentGroup) {
        Resident resident = residentRepository.findById(residentId).orElseThrow(CCheckResidentException::new);
        resident.putResidentRight(residentGroup);
        residentRepository.save(resident);
    }


}
