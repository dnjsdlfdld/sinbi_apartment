package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.Meeting;
import com.wonil.sinbiapartment.entity.News;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.enums.Category;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CCheckResidentException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.meeting.MeetingCategoryItem;
import com.wonil.sinbiapartment.model.meeting.MeetingDetail;
import com.wonil.sinbiapartment.model.meeting.MeetingListItem;
import com.wonil.sinbiapartment.model.meeting.MeetingRequest;
import com.wonil.sinbiapartment.model.news.NewsDetailItem;
import com.wonil.sinbiapartment.repository.MeetingRepository;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MeetingService {
    private final MeetingRepository meetingRepository;
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new);
        if (!resident.getIsEnable()) throw new CAccessDeniedException();
        return resident;
    }

    public Meeting getMeeting(long meetingId) {
        return meetingRepository.findById(meetingId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 모임 정보 등록하기 - 입주민용
     *
     * @param request MeetingRequest 등록 Form
     */
    public void setMeeting(MeetingRequest request) {
        Resident resident = getMemberData();
        Meeting meeting = new Meeting.MeetingBuilder(resident, request).build();
        meetingRepository.save(meeting);
    }

    /**
     * Enum의 리스트 가져오기
     *
     * @return MeetingCategoryItem 리스트의 정보 반환
     */
    public ListResult<MeetingCategoryItem> getCategories() {
        List<Category> categories = Arrays.stream(Category.values()).toList();
        List<MeetingCategoryItem> result = new LinkedList<>();
        categories.forEach(category -> {
            MeetingCategoryItem meetingCategoryItem = new MeetingCategoryItem.MeetingCategoryItemBuilder(category).build();
            result.add(meetingCategoryItem);
        });
        return ListConvertService.settingResult(result);

    }

    /**
     * 카테고리별 모임 게시글 가져오기
     *
     * @param category 아파트 소식 시퀀스
     * @return MeetingListItem list 정보
     */
    public ListResult<MeetingListItem> getMeetingList(Category category) {
        List<Meeting> meetings = meetingRepository.findAllByCategoryOrderByIdDesc(category);
        List<MeetingListItem> result = new LinkedList<>();
        meetings.forEach(meeting -> {
            MeetingListItem meetingListItem = new MeetingListItem.MeetingListItemBuilder(meeting).build();
            result.add(meetingListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 모임 게시글 상세정보 가져오기
     *
     * @param meetingId 아파트 소식 시퀀스
     * @return MeetingDetail 상세정보 Model
     */
    public MeetingDetail getMeetingDetail(long meetingId) {
        Meeting meeting = meetingRepository.findById(meetingId).orElseThrow(CCheckResidentException::new);
        return new MeetingDetail.MeetingDetailBuilder(meeting).build();
    }
}
