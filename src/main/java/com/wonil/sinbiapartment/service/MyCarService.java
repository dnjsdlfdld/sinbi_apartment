package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.MyCar;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.mycar.MyCarListItem;
import com.wonil.sinbiapartment.model.mycar.MyCarRequest;
import com.wonil.sinbiapartment.repository.MyCarRepository;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MyCarService {
    private final MyCarRepository myCarRepository;
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 세대주의 차량 등록하기
     *
     * @param resident 세대주 시퀀스
     * @param request  차량 등록 Model
     */
    public void setMyCar(Resident resident, MyCarRequest request) {
        MyCar myCar = new MyCar.MyCarBuilder(resident, request).build();
        myCarRepository.save(myCar);
    }

    /**
     * 나의 차량 리스트 가져오기
     *
     * @return MyCarListItem 차량 정보 반환
     */
    public ListResult<MyCarListItem> getMyCar() {
        Resident resident = getMemberData();
        List<MyCar> myCar = myCarRepository.findAllByResident_Id(resident.getId());
        List<MyCarListItem> result = new LinkedList<>();

        myCar.forEach(myCars -> {
            MyCarListItem myCarListItem = new MyCarListItem.MyCarListItemBuilder(myCars).build();
            result.add(myCarListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 관리자 -> 입주민의 차량 정보 가져오기
     *
     * @param residentId 회원 시퀀스
     * @return MyCarListItem 차량 정보 반환
     */
    public ListResult<MyCarListItem> getMyCarManager(long residentId) {
        List<MyCar> myCar = myCarRepository.findAllByResident_Id(residentId);
        List<MyCarListItem> result = new LinkedList<>();

        myCar.forEach(myCars -> {
            MyCarListItem myCarListItem = new MyCarListItem.MyCarListItemBuilder(myCars).build();
            result.add(myCarListItem);
        });
        return ListConvertService.settingResult(result);
    }
}
