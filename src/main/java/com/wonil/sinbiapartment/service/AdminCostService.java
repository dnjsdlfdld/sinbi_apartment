package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.AdminCost;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.admincost.AdminCostMonthItem;
import com.wonil.sinbiapartment.model.admincost.AdminCostRequest;
import com.wonil.sinbiapartment.repository.AdminCostRepository;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@RequiredArgsConstructor
@Service
public class AdminCostService {
    private final AdminCostRepository adminCostRepository;
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 관리자 -> 입주민 관리비 등록
     */
    public void setAdminCost(Resident resident, AdminCostRequest request, LocalDate dateStart, LocalDate dateEnd) {
        AdminCost adminCost = new AdminCost.AdminCostBuilder(resident, request, dateStart, dateEnd).build();
        adminCostRepository.save(adminCost);
    }

    /**
     * 입주민의 전월 관리비 가져오기
     *
     * @return AdminCostMonthItem 관리비 정보
     */
    public AdminCostMonthItem getMonth() {
        LocalDate getCostMonthStart = LocalDate.of(
                LocalDate.now().getYear(),
                LocalDate.now().getMonth(),
                1
        );

        Resident resident = getMemberData();
        AdminCost adminCost = adminCostRepository.findByResident_IdAndDateStart(
                resident.getId(), getCostMonthStart);

        if (adminCost == null) throw new CAccessDeniedException();

        AdminCostMonthItem result = new AdminCostMonthItem.AdminCostMonthItemBuilder(adminCost).build();
        return result;
    }
}
