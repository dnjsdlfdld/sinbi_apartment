package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.entity.VisitCar;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.visitcar.VisitCarListItem;
import com.wonil.sinbiapartment.model.visitcar.VisitCarRequest;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import com.wonil.sinbiapartment.repository.VisitCarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VisitCarService {
    private final VisitCarRepository visitCarRepository;
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 입주민이 신청할 방문차량 작성 메소드
     *
     * @param request 방문차량 신청 작성 Model
     */
    public void setVisitCar(VisitCarRequest request) {
        Resident resident = getMemberData();
        VisitCar visitCar = new VisitCar.VisitCarBuilder(resident, request).build();
        visitCarRepository.save(visitCar);
    }

    /**
     * 본인이 신청한 방문차량 리스트 가져오기
     *
     * @return 리스트의 내용을 보여줄 Model
     */
    public ListResult<VisitCarListItem> getVisitCarDetail() {
        Resident resident = getMemberData();
        List<VisitCar> visitCars = visitCarRepository.findAllByResident_Id(resident.getId());
        List<VisitCarListItem> result = new LinkedList<>();

        visitCars.forEach(visitCar -> {
            VisitCarListItem visitCarListItem = new VisitCarListItem.VisitCarListItemBuilder(visitCar).build();
            result.add(visitCarListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 입주민이 등록한 방문차량 승인하는 메소드
     *
     * @param visitCarId 방문차량 등록 시퀀스
     * @param isApproval 승인 or 반려 여부
     */
    public void putVisitCarApproval(long visitCarId, boolean isApproval) {
        VisitCar visitCar = visitCarRepository.findById(visitCarId).orElseThrow(CMissingDataException::new);
        visitCar.putVisitCarApproval(isApproval);
        visitCarRepository.save(visitCar);
    }

    public void delVisitCar(long visitCarId) {
        visitCarRepository.deleteById(visitCarId);
    }
}
