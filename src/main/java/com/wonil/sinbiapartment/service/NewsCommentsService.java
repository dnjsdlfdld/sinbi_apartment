package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.News;
import com.wonil.sinbiapartment.entity.NewsComments;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.newscomments.NewsCommentsItem;
import com.wonil.sinbiapartment.model.newscomments.NewsCommentsRequest;
import com.wonil.sinbiapartment.repository.NewsCommentsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NewsCommentsService {
    private final NewsCommentsRepository newsCommentsRepository;

    /**
     * 아파트 소식 댓글 작성하기
     *
     * @param resident 회원 시퀀스 record
     * @param news     아파트 소식 시퀀스 record
     * @param request  아파트 소식 작성 model
     */
    public void setNewsComments(Resident resident, News news, NewsCommentsRequest request) {
        NewsComments newsComments = new NewsComments.NewsCommentsBuilder(resident, news, request).build();
        newsCommentsRepository.save(newsComments);
    }

    /**
     * 아파트 소식 게시글 댓글 가져오기
     *
     * @param newsId 소식 게시글 시퀀스
     * @return NewsCommentsItem 댓글 Model
     */
    public ListResult<NewsCommentsItem> getNewsComments(long newsId) {
        List<NewsComments> findNewsComments = newsCommentsRepository.findAllByNews_IdOrderByIdDesc(newsId);
        List<NewsCommentsItem> result = new LinkedList<>();

        findNewsComments.forEach(newsComments -> {
            NewsCommentsItem newsCommentsItem = new NewsCommentsItem.NewsCommentsItemBuilder(newsComments).build();
            result.add(newsCommentsItem);
        });
        return ListConvertService.settingResult(result);
    }
}
