package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.News;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.enums.NewsState;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CCheckResidentException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.news.NewsDetailItem;
import com.wonil.sinbiapartment.model.news.NewsListItem;
import com.wonil.sinbiapartment.model.news.NewsRequest;
import com.wonil.sinbiapartment.repository.NewsRepository;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class NewsService {
    private final NewsRepository newsRepository;

    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 관리자가 아파트 공지사항 등록할 때
     * 관리자 권한이 아니라면 Exception 처리
     *
     * @param request 공지사항 등록 Model
     */
    public void setNews(NewsRequest request) {
        Resident resident = getMemberData();
        News news = new News.NewsBuilder(resident, request).build();
        newsRepository.save(news);
    }

    public News getNewsId(long newsId) {
        return newsRepository.findById(newsId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 아파트 소식 제목 리스트 가져오기
     *
     * @return NewsListItem 보여줄 Model
     */
    public ListResult<NewsListItem> getNewsList() {
        List<News> news = newsRepository.findAll();
        List<NewsListItem> result = new LinkedList<>();

        news.forEach(notice -> {
            NewsListItem newsListItem = new NewsListItem.NewsListItemBuilder(notice).build();
            result.add(newsListItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 아파트 소식 상세정보 가져오기
     *
     * @param newsId 아파트 소식 시퀀스
     * @return NewsDetailItem 상세정보 Model
     */
    public NewsDetailItem getNewsDetail(long newsId) {
        News news = newsRepository.findById(newsId).orElseThrow(CCheckResidentException::new);
        return new NewsDetailItem.NewsDetailItemBuilder(news).build();
    }

    /**
     * 아파트 소식 상태 변경하기
     *
     * @param newsId    아파트 소식 시퀀스
     * @param newsState 아파트 소식 상태 Enum
     */
    public void putNewsState(long newsId, NewsState newsState) {
        News news = newsRepository.findById(newsId).orElseThrow(CMissingDataException::new);
        news.putNewsState(newsState);
        newsRepository.save(news);
    }

}
