package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.*;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.meetingcomments.MeetingCommentsItem;
import com.wonil.sinbiapartment.model.meetingcomments.MeetingCommentsRequest;
import com.wonil.sinbiapartment.repository.MeetingCommentsRepository;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MeetingCommentsService {
    private final MeetingCommentsRepository meetingCommentsRepository;
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 모임 게시글에 대한 댓글 작성하기
     *
     * @param meeting 모임 게시글 시퀀스
     * @param request  댓글 작성 Model
     */
    public void setComplainComments(Meeting meeting, MeetingCommentsRequest request) {
        Resident resident = getMemberData();
        MeetingComments meetingComments = new MeetingComments.MeetingCommentsBuilder(resident, meeting, request).build();
        meetingCommentsRepository.save(meetingComments);
    }

    /**
     * 모임 게시글 시퀀스를 받아 해당 댓글 가져오기
     *
     * @param meetingId
     * @return MeetingCommentsItem 댓글 정보 model
     */
    public ListResult<MeetingCommentsItem> getMeetingComments(long meetingId) {
        List<MeetingComments> meetingComments = meetingCommentsRepository.findAllByMeeting_IdOrderByIdDesc(meetingId);
        List<MeetingCommentsItem> result = new LinkedList<>();

        meetingComments.forEach(meetingComment -> {
            MeetingCommentsItem meetingCommentsItem = new MeetingCommentsItem.MeetingCommentsItemBuilder(meetingComment).build();
            result.add(meetingCommentsItem);
        });
        return ListConvertService.settingResult(result);
    }
}
