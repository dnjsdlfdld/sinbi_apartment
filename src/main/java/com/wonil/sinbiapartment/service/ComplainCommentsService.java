package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.Complain;
import com.wonil.sinbiapartment.entity.ComplainComments;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.complaincomments.ComplainCommentsItem;
import com.wonil.sinbiapartment.model.complaincomments.ComplainCommentsRequest;
import com.wonil.sinbiapartment.repository.ComplainCommentsRepository;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ComplainCommentsService {
    private final ComplainCommentsRepository complainCommentsRepository;
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 민원 게시글에 대한 댓글 작성하기
     *
     * @param complain 민원 게시글 시퀀스
     * @param request  댓글 작성 Model
     */
    public void setComplainComments(Complain complain, ComplainCommentsRequest request) {
        Resident resident = getMemberData();
        ComplainComments complainComments = new ComplainComments.ServiceCounterCommentsBuilder(resident, complain, request).build();
        complainCommentsRepository.save(complainComments);
    }

    /**
     * 민원 게시글 시퀀스를 받아 해당 댓글 가져오기
     *
     * @param complainId
     * @return
     */
    public ListResult<ComplainCommentsItem> getComplainComments(long complainId) {
        List<ComplainComments> complainComments = complainCommentsRepository.findAllByComplain_IdOrderByIdDesc(complainId);
        List<ComplainCommentsItem> result = new LinkedList<>();

        complainComments.forEach(complainComment -> {
            ComplainCommentsItem complainCommentsItem = new ComplainCommentsItem.ComplainCommentsItemBuilder(complainComment).build();
            result.add(complainCommentsItem);
        });
        return ListConvertService.settingResult(result);
    }


}
