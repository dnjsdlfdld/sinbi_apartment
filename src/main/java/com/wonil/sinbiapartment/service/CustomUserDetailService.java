package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.exception.CUsernameSignInFailedException;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final ResidentRepository residentRepository;
    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CUsernameSignInFailedException::new);
        detailsChecker.check(resident);
        return resident;
    }
}
