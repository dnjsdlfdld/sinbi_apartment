package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.HouseMember;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.exception.CAccessDeniedException;
import com.wonil.sinbiapartment.exception.CMissingDataException;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.housemember.FamilyDetailItem;
import com.wonil.sinbiapartment.model.housemember.HouseMemberRequest;
import com.wonil.sinbiapartment.repository.HouseMemberRepository;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class HouseMemberService {
    private final HouseMemberRepository houseMemberRepository;
    private final ResidentRepository residentRepository;

    public Resident getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Resident resident = residentRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!resident.getIsEnable()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return resident;
    }

    /**
     * 세대주의 세대원 등록하기
     *
     * @param request 세대원 등록 Model
     */
    public void setHouseMember(HouseMemberRequest request) {
        Resident resident = getMemberData();
        HouseMember houseMember = new HouseMember.HouseMemberBuilder(resident, request).build();
        houseMemberRepository.save(houseMember);
    }

    /**
     * 세대원의 정보 가져오기
     *
     * @return FamilyDetailItem 세대원의 상세 정보 반환
     */
    public ListResult<FamilyDetailItem> getFamilyDetail() {
        Resident resident = getMemberData();
        List<HouseMember> houseMembers = houseMemberRepository.findAllByResident_Id(resident.getId());
        List<FamilyDetailItem> result = new LinkedList<>();

        houseMembers.forEach(houseMember -> {
            FamilyDetailItem familyDetailItem = new FamilyDetailItem.FamilyDetailItemBuilder(houseMember).build();
            result.add(familyDetailItem);
        });
        return ListConvertService.settingResult(result);
    }
}
