package com.wonil.sinbiapartment.service;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.enums.ResidentGroup;
import com.wonil.sinbiapartment.exception.CCheckEnableException;
import com.wonil.sinbiapartment.exception.CCheckResidentException;
import com.wonil.sinbiapartment.exception.CDoesNotMatchPasswordException;
import com.wonil.sinbiapartment.lib.CommonCheck;
import com.wonil.sinbiapartment.model.resident.ResidentRequest;
import com.wonil.sinbiapartment.repository.ResidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final ResidentRepository residentRepository;
    private final PasswordEncoder passwordEncoder;

    /**
     * DB Member 테이블에 아무 데이터가 없는 경우, 슈퍼관리자 아이디 생성
     */
    public void setFirstMember() {
        String username = "superadmin";
        String password = "idjh195233";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            ResidentRequest residentRequest = new ResidentRequest();
            residentRequest.setUsername(username);
            residentRequest.setPassword(password);
            residentRequest.setPasswordRe(password);
            residentRequest.setName("최고관리자");
            residentRequest.setAddress("서울특별시 강남구 반포대로");
            residentRequest.setApt_dong(111);
            residentRequest.setApt_number(4001);
            residentRequest.setPhone("010-6490-9031");

            setMember(ResidentGroup.ROLE_ADMIN, residentRequest);
        }
    }

    public void setMember(ResidentGroup residentGroup, ResidentRequest residentRequest) {
        if (!CommonCheck.checkUsername(residentRequest.getUsername())) throw new CCheckResidentException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!residentRequest.getPassword().equals(residentRequest.getPasswordRe())) throw new CDoesNotMatchPasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(residentRequest.getUsername())) throw new CCheckEnableException(); // 중복된 아이디가 존재합니다 던지기

        residentRequest.setPassword(passwordEncoder.encode(residentRequest.getPassword()));

        Resident resident = new Resident.ResidentBuilder(residentGroup, residentRequest).build();
        residentRepository.save(resident);
    }

    private boolean isNewUsername(String username) {
        long dupCount = residentRepository.countByUsername(username);
        return dupCount <= 0;
    }
}
