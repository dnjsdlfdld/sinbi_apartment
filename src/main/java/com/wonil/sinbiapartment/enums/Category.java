package com.wonil.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    FAMOUS_RESTAURANT("맛집", "우리동네 맛집을 이웃에게 공유해 주세요.", "assets/favorite_food.png"),
    EXERCISE("운동", "내 주변이 건강해야 모두가 행복합니다.", "assets/health.jpg"),
    SECONDHAND("중고마켓", "나에게 필요없는 물건이 누군가는 필요할 수 있어요!", "assets/market2.png"),
    BIKE_CLUB("바이크 동호회", "안전수칙을 준수해주세요.", "assets/bike.jpg");
    

    private final String name;
    private final String introduce;
    private final String imgUrl;
}
