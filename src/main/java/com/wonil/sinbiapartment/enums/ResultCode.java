package com.wonil.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다."),
    FAILED(-1, "실패하였습니다."),
    MISSING_DATA(-10000, "데이터를 찾을 수 없습니다."),
    DOES_NOT_MATCH_PASSWORD(-10001, "패스워드가 일치하지 않습니다."),

    ACCESS_DENIED(-1000, "권한이 없습니다."),
    USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다."),
    AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다."),

    DOES_NOT_MATCH_ACCOUNT(-10002, "아이디가 일치하지 않습니다."),
    CHECK_ADMIN(-10003, "권한을 확인해주세요"),
    CHECK_ENABLE(-10004, "탈퇴한 회원입니다."),
    CHECK_RESIDENT(-10005, "유효하지 않는 회원입니다."),
    CHECK_COMPLAIN_DETAIL(-10006, "유효하지 않은 게시물 입니다.");

    private final Integer code;
    private final String msg;
}