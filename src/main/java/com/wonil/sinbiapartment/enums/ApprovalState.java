package com.wonil.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApprovalState {
    BEFORE("처리전"),
    COMPLETE("완료"),
    COMPANION("반려");
    private final String name;
}
