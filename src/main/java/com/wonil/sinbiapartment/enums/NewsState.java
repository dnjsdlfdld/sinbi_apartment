package com.wonil.sinbiapartment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NewsState {
    PLAN("예정"),
    ONGOING("진행중"),
    COMPLETE("완료");

    private final String name;
}
