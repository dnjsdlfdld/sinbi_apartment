package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.enums.ResidentGroup;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.SingleResult;
import com.wonil.sinbiapartment.model.resident.MyInfoItem;
import com.wonil.sinbiapartment.model.resident.ResidentNameItem;
import com.wonil.sinbiapartment.model.resident.ResidentRequest;
import com.wonil.sinbiapartment.service.MemberDataService;
import com.wonil.sinbiapartment.service.ResidentService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "입주민 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/resident")
public class ResidentController {
    private final ResidentService residentService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "입주민 등록")
    @PostMapping("/new")
    public CommonResult setResident(@RequestParam ResidentGroup residentGroup, @RequestBody @Valid ResidentRequest request) {
        memberDataService.setMember(residentGroup, request);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "내 정보 가져오기")
    @GetMapping("/my-info")
    public SingleResult<MyInfoItem> getMyInfo() {
        return ResponseService.getSingleResult(residentService.getMyInfo());
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "residentId", value = "회원 등록번호", required = true)
    })
    @ApiOperation(value = "권한 수정하기")
    @PutMapping("/resident-group/{residentId}")
    public CommonResult putResidentAdmin(@PathVariable long residentId, @RequestParam ResidentGroup residentGroup) {
        residentService.putResidentAdmin(residentId, residentGroup);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "이름 가져오기 -> 로그인 후 띄워줄 용도")
    @GetMapping("/name")
    public SingleResult<ResidentNameItem> getName() {
        return ResponseService.getSingleResult(residentService.getName());
    }
}
