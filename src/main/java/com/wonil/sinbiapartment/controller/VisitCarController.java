package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.visitcar.VisitCarListItem;
import com.wonil.sinbiapartment.model.visitcar.VisitCarRequest;
import com.wonil.sinbiapartment.service.ResidentService;
import com.wonil.sinbiapartment.service.ResponseService;
import com.wonil.sinbiapartment.service.VisitCarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "방문차량 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/visit-car")
public class VisitCarController {
    private final VisitCarService visitCarService;


    @ApiOperation(value = "방문차량 등록")
    @PostMapping("/new")
    public CommonResult setVisitCar(@RequestBody @Valid VisitCarRequest request) {
        visitCarService.setVisitCar(request);

        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "방문차량 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<VisitCarListItem> getVisitCarDetail() {
        return ResponseService.getListResult(visitCarService.getVisitCarDetail(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "visitCarId", value = "방문차량 시퀀스", required = true)
    })
    @ApiOperation(value = "방문차량 신청 결재")
    @PutMapping("/approval/{visitCarId}")
    public CommonResult putVisitCarApproval(@PathVariable long visitCarId, @RequestParam boolean isApproval) {
        visitCarService.putVisitCarApproval(visitCarId, isApproval);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "visitCarId", value = "방문차량 시퀀스", required = true)
    })
    @ApiOperation(value = "신청 취소하기")
    @DeleteMapping("/visit-car-id/{visitCarId}")
    public CommonResult delVisitCar(@PathVariable long visitCarId) {
        visitCarService.delVisitCar(visitCarId);
        return ResponseService.getSuccessResult();
    }

}
