package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.SingleResult;
import com.wonil.sinbiapartment.model.admincost.AdminCostMonthItem;
import com.wonil.sinbiapartment.model.admincost.AdminCostRequest;
import com.wonil.sinbiapartment.service.AdminCostService;
import com.wonil.sinbiapartment.service.ResidentService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "관리비 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/admin-cost")
public class AdminCostController {
    private final AdminCostService adminCostService;
    private final ResidentService residentService;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "residentId", value = "회원 등록번호", required = true)
    })
    @ApiOperation(value = "관리비 등록 - 관리자용")
    @PostMapping("/new/{residentId}")
    public CommonResult setAdminCost(
            @PathVariable long residentId,
            @RequestBody @Valid AdminCostRequest request,
            @RequestParam("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateStart,
            @RequestParam("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateEnd
            ) {
        Resident resident = residentService.getResidentManager(residentId);
        adminCostService.setAdminCost(resident, request, dateStart, dateEnd);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "전월 관리비 '월' 가져오기 - 입주민용")
    @GetMapping("/cost/month")
    public SingleResult<AdminCostMonthItem> getMonth() {
        return ResponseService.getSingleResult(adminCostService.getMonth());
    }
}
