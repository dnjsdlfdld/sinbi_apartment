package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.entity.Complain;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.complaincomments.ComplainCommentsItem;
import com.wonil.sinbiapartment.model.complaincomments.ComplainCommentsRequest;
import com.wonil.sinbiapartment.service.ComplainCommentsService;
import com.wonil.sinbiapartment.service.ComplainService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "민원 게시글 댓글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/complain-comments")
public class ComplainCommentsController {
    private final ComplainService complainService;
    private final ComplainCommentsService complainCommentsService;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", value = "민원 게시글 등록번호", required = true)
    })
    @ApiOperation(value = "민원 게시글 댓글 작성")
    @PostMapping("/resident/complain/{complainId}")
    public CommonResult setComplainComments(
            @PathVariable long complainId,
            @RequestBody @Valid ComplainCommentsRequest request) {

        Complain complain = complainService.getComplain(complainId);
        complainCommentsService.setComplainComments(complain, request);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", value = "민원 게시글 등록번호", required = true),
    })
    @ApiOperation(value = "민원 게시글 댓글 가져오기")
    @GetMapping("/comments/{complainId}")
    public ListResult<ComplainCommentsItem> getComplainComments(@PathVariable long complainId) {
        Complain complain = complainService.getComplain(complainId);
        return ResponseService.getListResult(complainCommentsService.getComplainComments(complain.getId()), true);
    }
}
