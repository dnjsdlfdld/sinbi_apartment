package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.entity.Meeting;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.meetingcomments.MeetingCommentsItem;
import com.wonil.sinbiapartment.model.meetingcomments.MeetingCommentsRequest;
import com.wonil.sinbiapartment.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "모임 게시글 댓글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/meeting-comments")
public class MeetingCommentsController {
    private final MeetingService meetingService;
    private final MeetingCommentsService meetingCommentsService;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "meetingId", value = "모임 게시글 등록번호", required = true)
    })
    @ApiOperation(value = "모임 게시글 댓글 작성")
    @PostMapping("/resident/meeting/{meetingId}")
    public CommonResult setComplainComments(
            @PathVariable long meetingId,
            @RequestBody @Valid MeetingCommentsRequest request) {

        Meeting meeting = meetingService.getMeeting(meetingId);
        meetingCommentsService.setComplainComments(meeting, request);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "meetingId", value = "모임 게시글 등록번호", required = true),
    })
    @ApiOperation(value = "모임 게시글 댓글 가져오기")
    @GetMapping("/comments/{meetingId}")
    public ListResult<MeetingCommentsItem> getComplainComments(@PathVariable long meetingId) {
        Meeting meeting = meetingService.getMeeting(meetingId);
        return ResponseService.getListResult(meetingCommentsService.getMeetingComments(meeting.getId()), true);
    }
}
