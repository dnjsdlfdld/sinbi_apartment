package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.housemember.FamilyDetailItem;
import com.wonil.sinbiapartment.model.housemember.HouseMemberRequest;
import com.wonil.sinbiapartment.service.HouseMemberService;
import com.wonil.sinbiapartment.service.ResidentService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "세대원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/house-member")
public class HouseMemberController {
    private final HouseMemberService houseMemberService;


    @ApiOperation(value = "세대원 등록")
    @PostMapping("/new")
    public CommonResult setHouseMember(@RequestBody @Valid HouseMemberRequest request) {
        houseMemberService.setHouseMember(request);

        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "가족 구성원 이름 가져오기")
    @GetMapping("/family-detail")
    public ListResult<FamilyDetailItem> getFamilyDetail() {
        return ResponseService.getListResult(houseMemberService.getFamilyDetail(), true);
    }
}
