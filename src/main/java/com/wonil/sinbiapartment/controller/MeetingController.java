package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.enums.Category;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.SingleResult;
import com.wonil.sinbiapartment.model.meeting.MeetingCategoryItem;
import com.wonil.sinbiapartment.model.meeting.MeetingDetail;
import com.wonil.sinbiapartment.model.meeting.MeetingListItem;
import com.wonil.sinbiapartment.model.meeting.MeetingRequest;
import com.wonil.sinbiapartment.service.MeetingService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "모임관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/meeting")
public class MeetingController {
    private final MeetingService meetingService;

    @ApiOperation(value = "게시글 등록 - 입주민용")
    @PostMapping("/new")
    public CommonResult setMeeting(@RequestBody @Valid MeetingRequest request) {
        meetingService.setMeeting(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "카테고리 목록 가져오기")
    @GetMapping("/category")
    public ListResult<MeetingCategoryItem> getCategories() {
        return ResponseService.getListResult(meetingService.getCategories(), true);
    }

    @ApiOperation(value = "카테고리에 해당하는 리스트 가져오기")
    @GetMapping("/category/name")
    public ListResult<MeetingListItem> getMeetingList(@RequestParam Category category) {
        return ResponseService.getListResult(meetingService.getMeetingList(category), true);
    }
    @ApiOperation(value = "모임 게시글 상세정보 가져오기")
    @GetMapping("/category/{meetingId}")
    public SingleResult<MeetingDetail> getMeetingDetail(@PathVariable long meetingId) {
        return ResponseService.getSingleResult(meetingService.getMeetingDetail(meetingId));
    }
}
