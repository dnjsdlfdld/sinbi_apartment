package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.enums.ServiceState;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.SingleResult;
import com.wonil.sinbiapartment.model.complain.ComplainDetailItem;
import com.wonil.sinbiapartment.model.complain.ComplainListItem;
import com.wonil.sinbiapartment.model.complain.ComplainRequest;
import com.wonil.sinbiapartment.service.ComplainService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "민원 게시글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/complain")
public class ComplainController {
    private final ComplainService complainService;


    @ApiOperation(value = "민원 등록")
    @PostMapping("/new")
    public CommonResult setComplain(@RequestBody @Valid ComplainRequest request) {
        complainService.setComplain(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "민원 리스트 출력(공용)")
    @GetMapping("/all")
    public ListResult<ComplainListItem> getComplainList(@RequestParam(value = "title", required = false) String title) {
        if (title == null) {
            return ResponseService.getListResult(complainService.getComplainList(), true);
        } else {
            return ResponseService.getListResult(complainService.getComplainListName(title), true);
        }
    }

    @ApiOperation(value = "접수 상태 리스트 출력(공용)")
    @GetMapping("/received")
    public ListResult<ComplainListItem> getComplainReceivedState() {
        return ResponseService.getListResult(complainService.getComplainReceivedState(), true);
    }

    @ApiOperation(value = "진행중 상태 리스트 출력(공용)")
    @GetMapping("/ongoing")
    public ListResult<ComplainListItem> getComplainOngoingState() {
        return ResponseService.getListResult(complainService.getComplainOngoingState(), true);
    }

    @ApiOperation(value = "완료 상태 리스트 출력(공용)")
    @GetMapping("/complete")
    public ListResult<ComplainListItem> getComplainCompleteState() {
        return ResponseService.getListResult(complainService.getComplainCompleteState(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", value = "민원 게시글 등록번호", required = true)
    })
    @ApiOperation(value = "민원 Detail 가져오기(공용)")
    @GetMapping("/detail/{complainId}")
    public SingleResult<ComplainDetailItem> getComplainDetail(@PathVariable long complainId) {
        return ResponseService.getSingleResult(complainService.getComplainDetail(complainId));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", value = "민원 게시글 등록번호", required = true)
    })
    @ApiOperation(value = "민원 게시글 상태 수정하기")
    @PutMapping("/service-state/{complainId}")
    public CommonResult putServiceState(@PathVariable long complainId, @RequestParam ServiceState serviceState) {
        complainService.putComplainState(complainId, serviceState);
        return ResponseService.getSuccessResult();
    }
}
