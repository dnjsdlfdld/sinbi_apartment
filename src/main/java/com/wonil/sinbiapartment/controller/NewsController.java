package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.enums.NewsState;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.SingleResult;
import com.wonil.sinbiapartment.model.news.NewsDetailItem;
import com.wonil.sinbiapartment.model.news.NewsListItem;
import com.wonil.sinbiapartment.model.news.NewsRequest;
import com.wonil.sinbiapartment.service.NewsService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "아파트 소식 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/news")
public class NewsController {
    private final NewsService newsService;


    @ApiOperation(value = "아파트 소식 등록")
    @PostMapping("/new")
    public CommonResult setNews(@RequestBody @Valid NewsRequest request) {
        newsService.setNews(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "아파트 소식 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<NewsListItem> getNewsList() {
        return ResponseService.getListResult(newsService.getNewsList(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsId", value = "아파트 소식 등록번호", required = true)
    })
    @ApiOperation(value = "아파트 소식 상세정보 가져오기")
    @GetMapping("/detail/{newsId}")
    public SingleResult<NewsDetailItem> getNewsDetail(@PathVariable long newsId) {
        return ResponseService.getSingleResult(newsService.getNewsDetail(newsId));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsId", value = "아파트 소식 등록번호", required = true)
    })
    @ApiOperation(value = "아파트 소식 상태 변경하기")
    @PutMapping("/news-state/{newsId}")
    public CommonResult putNewsState(@PathVariable long newsId, @RequestParam NewsState newsState) {
        newsService.putNewsState(newsId, newsState);
        return ResponseService.getSuccessResult();
    }
}
