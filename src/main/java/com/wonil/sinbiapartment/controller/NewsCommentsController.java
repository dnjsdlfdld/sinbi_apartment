package com.wonil.sinbiapartment.controller;


import com.wonil.sinbiapartment.entity.News;
import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.newscomments.NewsCommentsItem;
import com.wonil.sinbiapartment.model.newscomments.NewsCommentsRequest;
import com.wonil.sinbiapartment.service.NewsCommentsService;
import com.wonil.sinbiapartment.service.NewsService;
import com.wonil.sinbiapartment.service.ResidentService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "아파트 소식 게시글 댓글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/news-comments")
public class NewsCommentsController {
    private final NewsService newsService;
    private final NewsCommentsService newsCommentsService;
    private final ResidentService residentService;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsId", value = "아파트 소식 게시글 등록번호", required = true)
    })
    @ApiOperation(value = "게시물 댓글 작성")
    @PostMapping("/resident/news/{newsId}")
    public CommonResult setNewsComments(
            @PathVariable long newsId,
            @RequestBody @Valid NewsCommentsRequest request) {
        Resident resident = residentService.getResident();
        News news = newsService.getNewsId(newsId);
        newsCommentsService.setNewsComments(resident, news, request);

        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsId", value = "아파트 소식 게시글 등록번호", required = true)
    })
    @ApiOperation(value = "게시글 댓글 가져오기")
    @GetMapping("/all/{newsId}")
    public ListResult<NewsCommentsItem> getNewsComments(@PathVariable long newsId) {
        return ResponseService.getListResult(newsCommentsService.getNewsComments(newsId), true);
    }
}
