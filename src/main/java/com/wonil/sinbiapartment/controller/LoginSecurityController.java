package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.enums.ResidentGroup;
import com.wonil.sinbiapartment.model.SingleResult;
import com.wonil.sinbiapartment.model.resident.LoginRequest;
import com.wonil.sinbiapartment.model.resident.LoginSecurityResponse;
import com.wonil.sinbiapartment.service.LoginService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginSecurityController {
    private final LoginService loginService;

    @ApiOperation(value = "웹 - 관리자 로그인")
    @PostMapping("/web/admin")
    public SingleResult<LoginSecurityResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(ResidentGroup.ROLE_ADMIN, loginRequest, "WEB"));
    }

    @ApiOperation(value = "앱 - 일반유저 로그인")
    @PostMapping("/app/user")
    public SingleResult<LoginSecurityResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(ResidentGroup.ROLE_USER, loginRequest, "APP"));
    }
}
