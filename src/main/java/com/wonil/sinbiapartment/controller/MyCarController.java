package com.wonil.sinbiapartment.controller;

import com.wonil.sinbiapartment.entity.Resident;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.model.ListResult;
import com.wonil.sinbiapartment.model.mycar.MyCarListItem;
import com.wonil.sinbiapartment.model.mycar.MyCarRequest;
import com.wonil.sinbiapartment.service.MyCarService;
import com.wonil.sinbiapartment.service.ResidentService;
import com.wonil.sinbiapartment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "우리집 자동차 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/my-car")
public class MyCarController {
    private final MyCarService myCarService;
    private final ResidentService residentService;


    @ApiImplicitParams({
            @ApiImplicitParam(name = "residentId", value = "회원 등록번호", required = true)
    })
    @ApiOperation(value = "우리집 자동차 등록")
    @PostMapping("/new/{residentId}")
    public CommonResult setMyCar(@PathVariable long residentId, @RequestBody @Valid MyCarRequest request) {
        Resident resident = residentService.getResidentManager(residentId);
        myCarService.setMyCar(resident, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "입주민의 자동차 목록 가져오기 - 입주민용")
    @GetMapping("/resident/car-all")
    public ListResult<MyCarListItem> getMyCar() {
        return ResponseService.getListResult(myCarService.getMyCar(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "residentId", value = "회원 등록번호", required = true)
    })
    @ApiOperation(value = "입주민의 자동차 목록 가져오기 - 관리자용")
    @GetMapping("/manager/{residentId}")
    public ListResult<MyCarListItem> getMyCar(@PathVariable long residentId) {
        return ResponseService.getListResult(myCarService.getMyCarManager(residentId), true);
    }

}
