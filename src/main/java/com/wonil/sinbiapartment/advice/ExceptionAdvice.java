package com.wonil.sinbiapartment.advice;

import com.wonil.sinbiapartment.enums.ResultCode;
import com.wonil.sinbiapartment.exception.*;
import com.wonil.sinbiapartment.model.CommonResult;
import com.wonil.sinbiapartment.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

// 조언자
@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) { // 기본적인 출구
        return ResponseService.getFailResult(ResultCode.FAILED); // 출구니까 FAILED
        // 감지못한 에러가 발생 시 400 BAD REQUEST 로 출력 기본 출구로 빠져나감
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CDoesNotMatchAccountException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDoesNotMatchAccountException e) {
        return ResponseService.getFailResult(ResultCode.DOES_NOT_MATCH_ACCOUNT);
    }

    @ExceptionHandler(CDoesNotMatchPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDoesNotMatchPasswordException e) {
        return ResponseService.getFailResult(ResultCode.DOES_NOT_MATCH_PASSWORD);
    }

    @ExceptionHandler(CCheckAdminException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CCheckAdminException e) {
        return ResponseService.getFailResult(ResultCode.CHECK_ADMIN);
    }

    @ExceptionHandler(CCheckEnableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CCheckEnableException e) {
        return ResponseService.getFailResult(ResultCode.CHECK_ENABLE);
    }

    @ExceptionHandler(CCheckResidentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CCheckResidentException e) {
        return ResponseService.getFailResult(ResultCode.CHECK_RESIDENT);
    }

    @ExceptionHandler(CCheckComplainDetailException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CCheckComplainDetailException e) {
        return ResponseService.getFailResult(ResultCode.CHECK_COMPLAIN_DETAIL);
    }
}
